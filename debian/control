Source: golang-github-containers-dnsname
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Reinhard Tartler <siretart@tauware.de>,
           Leo Antunes <costela@debian.org>
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-appc-cni-dev (>= 1.0.1~),
               golang-github-containernetworking-plugins-dev,
               golang-github-coreos-go-iptables-dev,
               golang-github-onsi-ginkgo-dev,
               golang-github-pkg-errors-dev,
               golang-github-sirupsen-logrus-dev,
               golang-github-vishvananda-netlink-dev,
               golang-golang-x-sys-dev,
               golang-gomega-dev
Standards-Version: 4.6.0.1
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-containers-dnsname
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-containers-dnsname.git
Homepage: https://github.com/containers/dnsname
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/containers/dnsname

Package: golang-github-containernetworking-plugin-dnsname
Architecture: any
Depends: dnsmasq-base,
         ${misc:Depends},
         ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Enhances: podman
Description: name resolution for containers
 This CNI plugin sets up the use of dnsmasq on a given CNI network so that Pods
 can resolve each other by name. When configured, the pod and its IP address
 are added to a network specific hosts file that dnsmasq reads in.  Similarly,
 when a pod is removed from the network, it will remove the entry from the
 hosts file.  Each CNI network will have its own dnsmasq instance.
 .
 The dnsname plugin was specifically designed for the Podman
 container engine.
